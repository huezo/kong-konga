# Kong in Docker Compose

This is the official Docker Compose template for [Kong][kong-site-url]. This implementation has Konga (Kong GUI).

# What is Kong?

You can find the official Docker distribution for Kong at [https://store.docker.com/images/kong](https://store.docker.com/images/kong).

# How to use this template

This Docker Compose template provisions a Kong container with a Postgres database, plus a nginx load-balancer. After running the template, the `nginx-lb` load-balancer will be the entrypoint to Kong.


# Install Docker 

[https://gitlab.com/-/snippets/2019153](https://gitlab.com/-/snippets/2019153)


To run this template execute:

```shell
$ docker-compose up
```

To scale Kong (ie, to three instances) execute:

```shell
$ docker-compose scale kong=3
```

In case of error with COMPOSE_HTTP_TIMEOUT: run on this way:

```shell
$ COMPOSE_HTTP_TIMEOUT=3600 docker-compose up
```

Konga Connection With Kong

```shell
Name: Kong
Kong Admin URL: http://kong:8001/
```

Kong will be available through the `nginx-lb` instance on port `8000`, and `8001`. You can customize the template with your own environment variables or datastore configuration.

Kong's documentation can be found at [https://docs.konghq.com/][kong-docs-url].




[kong-site-url]: https://konghq.com/
[kong-docs-url]: https://docs.konghq.com/
[github-new-issue]: https://github.com/Kong/docker-kong/issues/new
